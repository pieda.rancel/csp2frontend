import React, {useEffect, useContext} from 'react'
import {Container} from '@material-ui/core'
import Login from '../components/Login'
import UserContext from '../UserContext'
import Router from 'next/router'

export default function index() {

const {user} = useContext(UserContext)

useEffect(()=> {
	if(user.id !== null){
		Router.push('/home')
	}
},[user.id])

  return (
    <Container maxWidth="xl" disableGutters={true}>
      <Login />
    </Container>
  )
}
