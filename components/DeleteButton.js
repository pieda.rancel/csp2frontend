import React, {useState, useContext} from 'react'
import {Tooltip, IconButton} from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/Delete';
import Swal from 'sweetalert2'
import UserContext from '../UserContext'


export default function DeleteButton({record, setRecords}) {

	const {user, setBalance} = useContext(UserContext)

	const clickedDelete= () => {
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/records/${record}`, {
			method: 'DELETE',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/${user.id}`, {
					headers: {
						Authorization: `Bearer ${localStorage.getItem('token')}`
					}
				})
				.then(res => res.json())
				.then(data => {
					console.log(data, "deleteButton")

					const computeActiveBalance = () => {
 				const activeRecords = data.records.filter(record => record.isActive=== true)
 				setRecords(activeRecords)
 				return activeRecords.reduce((accumulatedValue, currentRecord) => {
 					if(currentRecord.type === "Expense"){
 						return +accumulatedValue - +currentRecord.amount
 					} else {
 						return +accumulatedValue + +currentRecord.amount
 					}
 				}, 0)
 			}

					setBalance(computeActiveBalance())
				})
		}})
	}


	return (
		    <Tooltip title="Delete" placement="left">
                <IconButton aria-label="delete" onClick={clickedDelete}>
                    <DeleteIcon />
                </IconButton>
            </Tooltip>
	)
}