import React, {useState, useEffect, useContext} from 'react'
import DeleteButton from './DeleteButton'
import EditButton from './EditButton'
import {TextField, MenuItem, Button} from '@material-ui/core'
import UserContext from '../UserContext'

export default function TableRow({record, setRecords}) {

	// console.log(record, "record")
	const {setBalance} = useContext(UserContext)
	const [canEdit, setCanEdit] = useState(false)
	const [categories, setCategories] = useState([])
	const [name, setName] = useState(record.name)
    const [categoryId, setCategoryId] = useState(record.categoryId)
    const [type, setType] = useState(record.type)
    const [amount, setAmount] = useState(record.amount)
    const [description, setDescription] = useState(record.description)

	useEffect(()=> {
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/categories/`)
		.then(res => res.json())
		.then(data => {
			setCategories(data)
			setCanEdit(false)
		})
	}, [])


  		const handleOnChange = (e) => {
			if(e.target.name === "recordName"){
				setName(e.target.value)
			} else if (e.target.name === "categoryId") {
				setCategoryId(e.target.value)
			} else if (e.target.name === "categoryType"){
				setType(e.target.value)
			} else if (e.target.name === "amount") {
				setAmount(e.target.value)
			} else if (e.target.name === "description") {
				setDescription(e.target.value)
			}

			console.log(name)
		}

	
		const handleSubmit = () => {
			fetch(`http://localhost:4000/api/records/${record._id}`, {
				method: "PUT",
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					name,
					categoryId,
					type,
					amount,
					description
				})
			})
			.then(res => res.json())
			.then(data => {
				
				const computeActiveBalance = () => {
 				const activeRecords = data.records.filter(record => record.isActive=== true)
 				return activeRecords.reduce((accumulatedValue, currentRecord) => {
 					if(currentRecord.type === "Expense"){
 						return +accumulatedValue - +currentRecord.amount
 					} else {
 						return +accumulatedValue + +currentRecord.amount
 					}
 				}, 0)
 			}
 				setBalance(computeActiveBalance())
 				setRecords(data.records.filter(record => record.isActive === true))
 				setCanEdit(false)
			})
		}

	return (
		<>
		<tr key={record._id}>
                <td className = "tableData">
                  <div className="actionDiv h-100">
                      <DeleteButton record = {record._id} setRecords={setRecords}/>
                      <EditButton 
                      canEdit = {canEdit} 
                      setCanEdit = {setCanEdit}
                      setName = {setName}
                      name = {name}
                      type = {type}
                      categoryId = {categoryId}
                      amount = {amount}
                      description = {description}
                      setCategoryId = {setCategoryId}
                      setType = {setType}
                      setAmount = {setAmount}
                      setDescription = {setDescription}
                      />
                      { 
                      	canEdit && <Button onClick={handleSubmit}>Submit</Button>
                      }
                  </div>
                </td>
                <td className="tableData">
                {
                  canEdit ? <TextField onChange = {handleOnChange} name="recordName" label = "Record Name" defaultValue={record.name}></TextField> : record.name
                }
                </td>
                <td className="tableData">
                {
                  canEdit 
                  ? <TextField
					defaultValue = {record.categoryId._id}
					name="categoryId"
					onChange = {handleOnChange}
					select
					label="Category">
							 { 
								categories.map(category => {
								return (
										<MenuItem key = {category._id} value={category._id}>{category.name}</MenuItem>
									)
							}) 
							}
					</TextField>

                  : record.categoryId.name
                }
                </td>
                <td className="tableData">
                { canEdit 
                	? <TextField defaultValue = {record.amount} onChange = {handleOnChange} name="amount" label="Amount" type="number" />
                  	: record.amount
                }
                </td>
                <td className="tableData">
                {	canEdit 
					? 	<TextField
						defaultValue = {record.type}
						  name = "categoryType"
						  label="Type"
						  onChange = {handleOnChange}
				          select>
							<MenuItem value="Expense">Expense</MenuItem>
							<MenuItem value="Income">Income</MenuItem>
						</TextField> 
                 : record.type
                }
                </td>
                <td className="tableData" style={{maxWidth: "40%"}}>
                {	canEdit 
					? <TextField 
						style = {{overflow: "hidden"}}
						defaultValue = {record.description}
						onChange = {handleOnChange}
						name = "description" 
						label = "description"
						/>
                  	:record.description
                }
                </td>
        </tr>
		</>
	)
}