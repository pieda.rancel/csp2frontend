import {useState, useEffect, useContext} from 'react'
import { Grid, Hidden, Typography, TextField, Button, Box} from '@material-ui/core'
import { GoogleLogin } from 'react-google-login'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import Link from 'next/link'
import {useStyles} from '../styleAPI'
import Register from './Register'
import Router from 'next/router'


export default function Login() {
  const {setUser} = useContext(UserContext)
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [isNew, setIsNew] = useState(false)
  const classes = useStyles()

  const authenticate = (e) => {
    e.preventDefault()
    console.log(email, password)
    fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email,
        password
      })
    })
    .then(res => res.json())
    .then(data => {
      if(data.accessToken){
       localStorage.setItem('token', data.accessToken)
      fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`,{
        headers: {
          authorization: `Bearer ${data.accessToken}`
        }
      })
      .then(res => res.json())
      .then(data => {
        if(data){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
        Router.push('/home')
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
      })
      }
    })
  }

    const handleRegisterState = (e) => {
      e.preventDefault()
      setIsNew(true)
    }



  const captureLoginResponse = (response) =>{
        console.log(response)
        const payload = {
            method: 'POST',
            headers: {
               'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                tokenId: response.tokenId
            })
        }

        fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/verify-google-id-token`, payload)
        .then(res => res.json())
        .then(data => {    
        console.log(data)       
            if(typeof data.accessToken !== 'undefined'){
                localStorage.setItem('token', data.accessToken)
                setUser({id: data._id, isAdmin: data.isAdmin})
                Router.push('/home')
            } else {
                /*else, if the data return by the api is an error msg*/
                if(data.error == 'google-auth-error'){
                    Swal.fire('Google Auth Error', 'Google authentication procedure failed.', error)
                } else if(data.error === 'login-type-error'){
                    Swal.fire('Login Type Error', 'You may have registered through a different login procedure', 'error')
                }
            }
        })
    }


  return (
    <Grid container direction="row" justify="center" alignItems="center" className={classes.root}>
          <Hidden only="xs">
            <Grid container item direction="row" justify="center" alignItems="center" sm={5} className={`${classes.grids} ${classes.left}`}>
              <img src="/login.svg" alt="" className={classes.svgColor}/>
            </Grid>
          </Hidden>
            <Grid container item xs={10} direction="column" justify="center" alignItems="center" sm={5} className={`${classes.grids} ${classes.right}`}>
              <img src="/logo.png" style={{height: "175px", width: "175px", borderRadius: "50%" }} alt=""/>
            { isNew === false ? <>
            <form onSubmit={authenticate} className={classes.form}>
              <TextField id="email" label="Email" onChange={(e)=>setEmail(e.target.value)} className={classes.textfield}/>
              <TextField id="password" onChange={(e)=>setPassword(e.target.value)} label="Password" type="password" className={classes.textfield}/>
                <Grid container item sm={12} direction="row" justify="center" alignItems="center">
                  <Button variant="contained" type="submit" color="primary">Login</Button>
                  <div className="w-100 text-primary"></div>
                  <small className="my-2 text-"><em>Not a member? <a style={{cursor: "pointer"}} onClick={handleRegisterState}><u>Register</u></a></em></small>
                </Grid>
            </form>
            <p style={{color: "black"}}>or Login/Register using</p>
           <GoogleLogin render={renderProps => (<button onClick={renderProps.onClick} disabled={renderProps.disabled} style={{cursor: "pointer", border: "none", backgroundImage: "url('/1x/btn_google_signin_dark_normal_web.png')", height:"40px", width:"184px", backgroundPosition: "center"}}></button>
    )} clientId="91423877803-qut6en98op7ds4i7jr30e9o104rvbi5m.apps.googleusercontent.com" onSuccess={captureLoginResponse} onFailure={captureLoginResponse} cookiePolicy={'single_host_origin'}/>
            </>
            : <Register setIsNew = {setIsNew}/>} 
            </Grid> 
      </Grid>
  )
}

      