import React, {useState} from 'react'
import {useStyles} from '../styleAPI'
import {TextField, Button} from '@material-ui/core'
import Swal from 'sweetalert2'
import Alert from '@material-ui/lab/Alert';


export default function Register({setIsNew}) {
	const classes = useStyles()

	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [emailWarning, setEmailWarning] = useState('')
	const [incomplete, setIncomplete] = useState('')
	
	const clickedRegister = (e) => {
		e.preventDefault()
		if(firstName === '' && lastName === '' && email === '' && password === ''){
			setIncomplete('Incomplete Registration Details')
		} else {
			setIncomplete('')
				const data = {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password
					})
				}

				fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/register`, data)
				.then(res => res.json())
				.then(serverData => {
					console.log(serverData)
					if(serverData){
						Swal.fire('Congratulations!', 'You can now login', 'success')
						setIsNew(false)
						setEmailWarning('')
					} else {
						setEmailWarning('*Duplicate email found')
					}
			})		
		}

	}

	const handleEmail = (e) => {
		if(e.target.value.indexOf('@', 0) !== -1 && e.target.value.indexOf('.com', 1) !== -1 || e.target.value === ''){
			setEmail(e.target.value)
			setEmailWarning('')
		} else {
			setEmailWarning('Invalid Email')
		}
	}


	return (
		<>
		<form onSubmit = {clickedRegister} className="d-flex flex-column align-items-center justify-content-center">
				<h3 className="text-danger">{incomplete}</h3>
                <TextField id="firstName" onChange={(e)=> {setFirstName(e.target.value)}} label="First Name" className={classes.textfield} />
                <TextField id="lastName" onChange={(e)=> {setLastName(e.target.value)}} name="lastName" label="Last Name" className={classes.textfield}/>
                <TextField id="email" onChange={handleEmail} name="email" label="Email" className={classes.textfieldEmail}/>
                <small className="w-100 mb-3"><span className="text-danger text-left">{emailWarning}</span></small>
                <TextField id="password" onChange={(e)=> {setPassword(e.target.value)}} name="password" label="Password" type="Password" className={classes.textfield}/>
                <Button type="submit" variant="contained" color="primary">Register</Button>
        </form>
        <div className="w-100"><em className="text-right d-block"><u className="mr-4" onClick={()=> setIsNew(false)} style={{cursor: "pointer"}}>Login</u></em></div>
        </>
	)
}