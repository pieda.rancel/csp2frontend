import React, {useContext, useState, useEffect, useRef} from 'react'
import {Navbar, Nav, Button, Col, Row} from 'react-bootstrap'
import Link from 'next/link'
import UserContext from '../UserContext'
import CountUp from 'react-countup';

   const usePreviousState = (value) => {
     const ref = useRef();
      useEffect(() => {
        ref.current = value;
      });
      return ref.current;
   }


export default function NavBar() {
  const {unsetUser, balance} = useContext(UserContext)
  const [colorBalance, setColorBalance] = useState('white')




  const previousBalance = usePreviousState(balance)

  useEffect(()=> {
    if(balance >= 1000){
      setColorBalance('white')
    } else if (balance < 1000 && balance >= 500){
      setColorBalance('orange')
    } else if (balance < 500 && balance > 0){
      setColorBalance('redOrange')
    } else {
      setColorBalance('red')
    }
  }, [balance])

  return (
    <>
  <style type="text/css">
    {`
    .bg-custom {
      background: linear-gradient(8deg, rgba(230,228,31,0.7231267507002801) 0%, rgba(255,0,0,1) 100%) !important;
      color: white;
      min-height: 10vh !important;
    }
    .nav-link {
      color: white !important;
      padding: 1rem 2rem !important;
      border-radius: 30px;
      text-align: center !important;
    }
    .nav-link:hover {
      box-shadow: inset 0px 0px 79px -26px rgba(255,255,255,1);
    },
    .navbarHeight {
      height: 50vh !important;
      align-items: center;
      justify-content: center;
      padding: 0 !important;
    }
    .w-custom {
      min-width: 40%;

    }
    .img-custom {
      position: absolute;
      bottom: 0;
      left: 0;
    }
    .posNav {
      position: absolute;
      right: 0;
      botom: 0;
    }
    .navMw {
      max-width: 80% !important;
      width: 70%;
      overflow: hidden;
    }
    .text-border {
       text-shadow: -1px 0 gray, 0 1px gray, 1px 0 gray, 0 -1px gray;
    }
    `}
  </style>


    <Navbar className="navbarHeight" bg="custom" expand="lg" fixed="bottom">
      <Link href="/home">
        <Navbar.Brand href="/home">
          <img
            src="/pig.png"
            width="50"
            height="50"
            className="d-inline-block img-custom"
          />
        </Navbar.Brand>
      </Link>
      <Nav className="navMw">
        <Row>
          <Col sm={6} className="text-center h4 d-flex text-border"><span className="ml-4 mr-1">Balance: </span><CountUp separator="," start={previousBalance} end={balance} /></Col>
        </Row>
      </Nav>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse>
      <Nav className="mr-auto text-white">
        <Link href="/home">
          <Nav.Link href="/">Records</Nav.Link>
        </Link>
        <Link href="/breakdown">
          <Nav.Link href="/">Breakdown</Nav.Link>
        </Link>
      </Nav>
      <Nav>
        <Nav.Link href="" onClick={unsetUser}>Logout</Nav.Link>
      </Nav>    
    </Navbar.Collapse>
    </Navbar>
    </>
  )
}